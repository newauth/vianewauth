# Add **Login with Newauth** button to your website

<img src="images/image-7.png" />

This project contains [Instructions](#instructions) to enable **Login with Newauth** for your website. Newauth is a new authentication method which works based on your visual memory. No need for passwords, OTPs or biometrics. 

You can learn more at our [site](https://newauth.io)


## Familiarize yourself first

There are two HTML files in this repo. 

    1. via-newauth.htm
    2. via-newauth-postlogin.htm

These files have the code which interacts with Newauth. Download these to your computer. 

Load 1 in your browser and you should see this page.

<img src="images/image-8.png" width="500" />

Clicking on 'Login with Newauth' button launches Newauth application.

<img src="images/image-1.png" width="500" />

### Login failure scenario

Type any random username and start clicking on the pictures shown. You will fail the authentication. 

### Login success scenario

If you have a newauth account, go ahead and use that.

If not, you may try **bottom-right** as the username. This is a special account where the secret location is the bottom right corner of the picture. Just click towards the last few pixels on the bottom right corner of each picture. As shown by the <span style="color:red">*red*</span> circle in this picture

<img src="images/image-2.png" width="500" />

After successful authentication, you will see this page (this is the HTML file no. 2). This page will display the user's details retrieved from Newauth. These will include user data and any Local Ids for this user. 

<img src="images/image-3.png" width="500" />

##### Local Id is a user's ID on your site [It could be different from their ID in Newauth]

Newauth refers to this as a Local Id in some demos or even external ID (extID). Usually you will not see a Local ID for a user you are testing with. You need to add a Local ID to a user's Newauth account before it will show up on this page. This is done with bindhostuserid API (described later).

## Instructions

There are two different use cases for logging into a site. The changes you need to make to your site will be driven by this choice.

1. This type of site needs to mainly keep track of transactions by the users. The main information flow in this use case is from the site to the user. There is comparatively less data flowing from the user to the site. Think a restaurant's order system. Apart from the order information, there is very little information flowing FROM the user in this case. These cases do not need a [Local Id](#local-id) for the user.

2. This type of site has a lot of data flowing from a user to the site. Think most content based social sites. These will typically need a Local Id for a user. Many times it is an email address of the user, other times, it is something else.

If your use case is similar to the second use case above (your users need a Local Id to access your site), then it is advised that you create a brand account for your website on Newauth before enabling it for Newauth Login. This will make sure that the Local Ids for your website are stored separately under your account. You will need a *flake* to create a Newauth account. Use **YgHhheJV2DpVRs6s**

#### Here are the steps

- [ ] In order to get the "Login with Newauth" button to display on your site, create a div with id 'via-newauth-button' in your HTML and place this div where you want the button to appear.

```
<div id="via-newauth-button"></div>
```
and include this script somewhere in your HTML. You could change the value of hostId to your flake if you created your brand account.

```
<script> 
(function (w,d,s,o,f,js,fjs) { w['VIANEWAUTH']=o;w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) }; js = d.createElement(s), fjs = d.getElementsByTagName(s)[0]; js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs); }(window, document, 'script', 'vn', 'https://newauth.io/static/js/viana/via_na.js')); 

vn('init', { hostId:'YgHhheJV2DpVRs6s', banner: '', showButton:true }); 
</script> 
```

- [ ] Your HTML page should display this button now. 

<img src="images/image-7.png" />

- [ ] Clicking the button will launch the Newauth app on your website. It will display this banner

<img src="images/image-6.png" width="300" />

You can change this text by passing an alternate text in the banner field. Like this

```
vn('init', { hostId:'YgHhheJV2DpVRs6s', banner: 'Howdy', showButton:true }); 
```

- [ ] Now, your site will allow users to launch the Newauth application, enter their Newauth credentials and Login to Newauth. We still need to get data from Newauth and login the user into YOUR system.

- [ ] Create a successhandler function and bind it to the Newauth API. You can use this template and add it to the script you added earlier

```
function callmewhenauthenticated () {
	console.log('Success Handler called');

	// This should be called when auth is successful
	console.log('Newauth user ' + JSON.stringify(vn.fns.getuserdata()));

	// Look for site specific IDs returned by Newauth
	console.log('Local IDs returned from Newauth ' + JSON.stringify(vn.fns.getuserdata().extIds));

	// If one site specific ID are returned by Newauth
	// Take that user to their page without asking them to login again..
	// .... your site code

	// If more than one site specific IDs are returned by Newauth
	// Have the user choose appropriate profile they want to access
	// .... your site code

	// If no site specific IDs are returned by Newauth and you DO NEED to use Local Ids	
	if (vn.fns.getuserdata().extIds == null) {
		console.log('No site specific credentials found');
		// Have the user login to your site as usual -- this is where the HTML 2 in this repo will come in handy
	}
	
	//.... your site code
	//window.location.href="vianewauthpostlogin.htm";
	
}
```
Here is how you bind this method
```
vn('successhandler', {callback: callmewhenauthenticated });
```
Now your logic will be called after the user logs into Newauth. 

- [ ] Create a failure handler and bind it to the API.

```
function callmewhenauthenticationFails () {
	// this should be called when the auth attempt fails
	// it is upto the website owner to decide - closing Via Newauth dialog is one option.
	// We are closing the Newauth dialog when failcount reaches 3
	console.log('Failure Handler called');
	if (failcount == 3) {		
		vn.fns.close();
		failcount = 0;
	} else {
		failcount ++;
	}
	
	// Any other action
	// ....
}
```
And bind it like you did with successhandler

```
vn('failurehandler', {callback: callmewhenauthenticationFails });
```

- [ ] That is the bulk of it. If you are only interested in making sure that a real person is logging to your site, you do not need to do anything more. You will get the details of the user in the successhandler. However, if you need to take your users to their own page, you need to bind their 'Local Id' to their Newauth account. Here is how to do it. All the code used below this point is available in the HTML 2 (via-newauth-postlogin.htm)

1. Let the user Login into Newauth as usual. 
2. In the successhandler, check for the the extIds returned by Newauth

```
    if (vn.fns.getuserdata().extIds == null) {
		console.log('No site specific credentials found');
		// Have the user login to your site as usual -- 
	}
```
3. Take the user to the Login screen of you site and have them login with their Local Id.
4. On the user's welcome page, include this script

```
<script> (function (w,d,s,o,f,js,fjs) { w['VIANEWAUTH']=o;w[o] = w[o] || function () { (w[o].q = w[o].q || []).push(arguments) }; js = d.createElement(s), fjs = d.getElementsByTagName(s)[0]; js.id = o; js.src = f; js.async = 1; fjs.parentNode.insertBefore(js, fjs); }(window, document, 'script', 'vn', 'https://newauth.io/static/js/viana/via_na.js')); 
	vn('init', { hostId:'YgHhheJV2DpVRs6s', banner: '', showButton:false, mode:'postauth' }); 
</script>
```
5. The above script will invoke Newauth in the **postauth** mode. Since the user is already logged into Newauth, we can add their Local Id to their Newauth account now. We do this with the following call. Change abcuser@somehost.com to the Local Id of the user, with which they login into your site.
```
vn('bindhostuserid',  { userId:'abcuser@somehost.com' });
```

Now you are all set. Here are some ideas about how to handle Local Ids once your users are able to successfully log into your site with Newauth.

- You could disable login with the Local Id. This will make the user's profiles more secure.
- You could keep the login with Local Id option open. This will give your users multiple options to log into your site.

